<?php declare(strict_types=1);

namespace Elpra\SimpleGitlabAPI\TextUI;

use PHPUnit\Framework\TestCase;
use Tests\GuzzleMock;

function getopt($options, $longopts) {
    return CommandTest::$getopt_longopts;
}

function runMain(array $options): array {
    CommandTest::$getopt_longopts = $options;
    $result = array('exit' => null, 'output' => null);    
    ob_start();
    $result['exit']   = Command::main();
    $result['output'] = ob_get_contents();
    ob_end_clean();    
    return $result;    
}

final class CommandTest extends TestCase
{
    const TEST_API_URL   = 'http://testserver/gitlab/api/v4/';
    const TEST_API_TOKEN = '123456789ABCDEFGH-xy';
    const COMMAND_EXEC   = 'php simplegitlabapi';
    
    private $help_menue_str;
    
    private $guzzle_get_mock;
    
    public static $getopt_longopts = null;
    
    protected function setUp(): void
    {
        $this->help_menue_str = file_get_contents('tests/command_help_menue.txt');
        $this->guzzle_get_mock = new \Tests\GuzzleMock();
    }

    public function testRunOnCommandLine(): void
    {
        $expected = $this->help_menue_str;
        $actual = shell_exec(self::COMMAND_EXEC);        
        $this->assertEquals($expected, $actual);
    }

    public function testShowHelp(): void
    {
        
        $tests = [
            'none arguments'  => ['arguments' => [],                    'result' => ['exit' => null, 'output' => $this->help_menue_str]],
            'help argument'   => ['arguments' => ['help' => false],     'result' => ['exit' => null, 'output' => $this->help_menue_str]],
            'false arguments' => ['arguments' => ['get' => 'projects'], 'result' => ['exit' => null, 'output' => $this->help_menue_str]]
        ];
        
        foreach ($tests as $test) {
            $main_result = runMain($test['arguments']);            
            $this->assertEquals($test['result']['exit'],   $main_result['exit']);
            $this->assertEquals($test['result']['output'], $main_result['output']);
        }
        
    }
    
    /**
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     */
    public function testGetCall(): void
    {

        $this->guzzle_get_mock = new GuzzleMock();
        $expected_api_call = $this->guzzle_get_mock->expected_api_call;
        
        CommandTest::$getopt_longopts = array(
            'get'   => $expected_api_call,
            'url'   => self::TEST_API_URL,
            'token' => self::TEST_API_TOKEN
        );
        
        $guzzle_mock = \Mockery::mock('overload:\GuzzleHttp\Client');
        $guzzle_mock->shouldReceive('get')
                    ->with($expected_api_call)
                    ->once()->andReturn($this->guzzle_get_mock);
       
        $command = new Command();
        
        ob_start();        
        $exit_code = $command->run();
        $actual = ob_get_contents();
        ob_end_clean();
        
        $expected = $this->guzzle_get_mock->expected_json_string_pretty.PHP_EOL;
        
        $this->assertEquals($exit_code, 0);
        $this->assertEquals($expected, $actual);
        
    }

    /**
     * @todo Add test fair failing arguments
     * 
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     */
    public function testPostCall(): void
    {
        
        $this->guzzle_get_mock = new GuzzleMock();
        $expected_api_call = $this->guzzle_get_mock->expected_api_call;
        $expected_arguments = array(
            'form_params' => []
        );
        
        CommandTest::$getopt_longopts = array(
            'post'      => $expected_api_call,
            'url'       => self::TEST_API_URL,
            'token'     => self::TEST_API_TOKEN,
            'arguments' => '{}'
        );
        
        $guzzle_mock = \Mockery::mock('overload:\GuzzleHttp\Client');
        $guzzle_mock->shouldReceive('post')
        ->with($expected_api_call, $expected_arguments)
        ->once()->andReturn($this->guzzle_get_mock);
        
        $command = new Command();
        
        ob_start();
        $exit_code = $command->run();
        $actual = ob_get_contents();
        ob_end_clean();
        
        $expected = $this->guzzle_get_mock->expected_json_string_pretty.PHP_EOL;
        
        $this->assertEquals($exit_code, 0);
        $this->assertEquals($expected, $actual);
        
    }
    
}
