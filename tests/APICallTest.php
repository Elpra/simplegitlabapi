<?php declare(strict_types=1);

namespace Elpra\SimpleGitlabAPI\API;

use PHPUnit\Framework\TestCase;

final class APICallTest extends TestCase
{
    const TEST_API_URL   = 'http://testserver/gitlab/api/v4/';
    const TEST_API_TOKEN = '123456789ABCDEFGH-xy';

    private $guzzle_mock;
    
    protected function setUp(): void
    {
        $this->guzzle_mock = new \Tests\GuzzleMock();
    }

    public function testBaseURL(): void
    {
               
        $API_URL_WITH_SLASH    = 'http://testserver/gitlab/api/v4/';
        $API_URL_WITHOUT_SLASH = 'http://testserver/gitlab/api/v4';
        
        $api = new APICall($API_URL_WITH_SLASH, self::TEST_API_TOKEN);
        $this->assertEquals($API_URL_WITH_SLASH, $api->getBaseURL());
        
        unset($api);
        
        $api = new APICall($API_URL_WITHOUT_SLASH, self::TEST_API_TOKEN);
        $this->assertEquals($API_URL_WITHOUT_SLASH.'/', $api->getBaseURL());
        
        unset($api);
        
    }
    
    public function testPrivateToken(): void
    {
        $api = new APICall(self::TEST_API_URL, self::TEST_API_TOKEN);
        $this->assertEquals(self::TEST_API_TOKEN, $api->getPrivateToken());
        unset($api);
    }

    /**
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     */
    public function testGetCall(): void
    {
        
        $api_call = $this->guzzle_mock->expected_api_call;

        $guzzle_mock = \Mockery::mock('overload:\GuzzleHttp\Client');
        $guzzle_mock->shouldReceive('get')->with($api_call)->once()->andReturn($this->guzzle_mock);
        
        $api = new \Elpra\SimpleGitlabAPI\API\APICall(self::TEST_API_URL, self::TEST_API_TOKEN);

        $this->assertEquals($this->guzzle_mock->expected_json_array,  $api->getCall($api_call)); 
        $this->assertEquals($this->guzzle_mock->expected_json_array,  $api->getCall($api_call, true));
        $this->assertEquals($this->guzzle_mock->expected_json_string, $api->getCall($api_call, false));
        
    }

    /**
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     */
    public function testPostCall(): void
    {
        
        $api_call = $this->guzzle_mock->expected_api_call;
        $arguments = array(
            'form_params' => []
        );
        
        $guzzle_mock = \Mockery::mock('overload:\GuzzleHttp\Client');
        $guzzle_mock->shouldReceive('post')->with($api_call, $arguments)->once()->andReturn($this->guzzle_mock);
        
        $api = new \Elpra\SimpleGitlabAPI\API\APICall(self::TEST_API_URL, self::TEST_API_TOKEN);
        
        $this->assertEquals($this->guzzle_mock->expected_json_array,  $api->postCall($api_call, []));
        $this->assertEquals($this->guzzle_mock->expected_json_array,  $api->postCall($api_call, [], true));
        $this->assertEquals($this->guzzle_mock->expected_json_string, $api->postCall($api_call, [], false));
        
    }
    
}

