<?php

namespace Tests;

class GuzzleMock
{
    
    const GET_REQUEST_EXAMPLE_FILE = "tests/example_get_request.json";
    
    public $expected_json_array         = null;
    public $expected_json_string        = null;
    public $expected_json_string_pretty = null;
    public $expected_api_call           = null;
    
    public function __construct()
    {
        
        $json = preg_replace('/\s+/', '', file_get_contents(self::GET_REQUEST_EXAMPLE_FILE));
        
        if ($this->isJSON($json)) {
            $this->expected_json_string = $json;
            $this->expected_json_string_pretty = json_encode(json_decode($json), JSON_PRETTY_PRINT);
            $this->expected_json_array  = json_decode($json, true);
            $this->expected_api_call    = "projects/".$this->expected_json_array['id'];
        } else {
            trigger_error("Invalid JSON string", E_USER_ERROR);
        }        
        
    }
    
    /**
     * @todo This return type should be a Stream interface! More informations see:
     * - http://php.net/manual/de/book.stream.php
     * - http://docs.guzzlephp.org/en/stable/psr7.html?highlight=getBody#body
     */
    public function getBody()
    {
        return $this->expected_json_string;
    }
    
    private function isJSON($json)
    {
        return (json_decode($json) === null) ? false : true;        
    }
    
}