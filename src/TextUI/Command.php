<?php

/**
 * Defines everything which is needed to run API Calls as CLI application.
 * 
 * @copyright Tobias Baumann <tobias.baumann@elpra.de>
 *
 * @license See LICENSE file in root directory
 */

namespace Elpra\SimpleGitlabAPI\TextUI;

use Elpra\SimpleGitlabAPI\API\APICall;

/**
 * Command Line Interface (CLI) Class
 */
class Command
{
    /**
     * All available options, with their definition and dependencies.
     */
    const LONG_OPTIONS = [
        'get'       => ['arg' => 'request',     'depends_on' => ['url', 'token'], 'desc' => 'Gitlab API GET Request'],
        'post'      => ['arg' => 'request',     'depends_on' => ['url', 'token'], 'desc' => 'Gitlab API POST Request'],
        'delete'    => ['arg' => 'request',     'depends_on' => ['url', 'token'], 'desc' => 'Gitlab API DELETE Request'],
        'arguments' => ['arg' => 'json string', 'depends_on' => null,             'desc' => 'Gitlab API POST Request Parameters'],
        'token'     => ['arg' => 'token',       'depends_on' => null,             'desc' => 'Gitlab API private token'],
        'url'       => ['arg' => 'url',         'depends_on' => null,             'desc' => 'Gitlab Server URL'],
        'version'   => ['arg' => null,          'depends_on' => null,             'desc' => 'Prints SimpleGitlabAPI version number and exits'],
        'help'      => ['arg' => null,          'depends_on' => null,             'desc' => 'Prints this help and exits']        
    ];
    
    /**
     * @var array Stores passed CLI arguments
     */
    private $cli_parameters = [];

    /**
     * Main function
     *
     * @return int Exit code
     */
    public static function main(): int
    {
        $command = new static;
        return $command->run();
    }

    /**
     * Runs API Call
     *
     * @return int Exit code
     */
    public function run(): int
    {
        $this->handleArguments();
        
        if (array_key_exists('get', $this->cli_parameters)) {
            $api_call = $this->createAPICall();
            return $this->handleGetCall($api_call);
        } elseif (array_key_exists('post', $this->cli_parameters)) {
            $api_call = $this->createAPICall();
            return $this->handlePostCall($api_call);
        } elseif (array_key_exists('delete', $this->cli_parameters)) {
            $api_call = $this->createAPICall();
            return $this->handleDeleteCall($api_call);
        }

        $this->showHelp();
        
        return 0;
    }

    /**
     * Create a API Call instance
     * 
     * @return APICall
     */
    private function createAPICall(): APICall
    {
        return new APICall($this->cli_parameters['url'], $this->cli_parameters['token']);
    }

    /**
     * Handling and checking arguments.
     *
     * @return bool True if fetching arguments and dependency check were valid, else False.
     */
    private function handleArguments(): bool
    {
        $valid_arguments = true;
        
        $longopts = [];
        
        foreach (self::LONG_OPTIONS as $key => $value) {            
            $longopts[] = ($value['arg'] === null) ? $key : $key."::";
        }
                
        $options = getopt("", $longopts);
        
        if (empty($options)) $valid_arguments = false;
        
        if (!$this->checkOptionDependencies($options)) {
            $valid_arguments = false;
        }
        
        if (array_key_exists('arguments', $options)) {
            
            $decoded_params = $this->decodeArgumentsString($options['arguments']);           
            unset($options['arguments']);
            
            if (!is_array($decoded_params)) {
                $valid_arguments = false;
                $options['arguments'] = [];
            } else {
                $options['arguments'] = $decoded_params;
            }
            
        } else {
            $options['arguments'] = [];
        }
 
        if (!$valid_arguments) {
            unset($options);
            $options = ['help' => false];
        }
        
        $this->cli_parameters = $options;
        
        return $valid_arguments;        
    }

    /**
     * Handles the GET API call
     * 
     * @param  APICall $api_call API Call Instance
     * @return int     Exit Code
     */
    private function handleGetCall(APICall $api_call): int
    {
        $result = $api_call->getCall($this->cli_parameters['get'], false);
        print json_encode(json_decode($result), JSON_PRETTY_PRINT);
        print \PHP_EOL;
        return 0;
    }

    /**
     * Handles the POST API call
     *
     * @param  APICall $api_call API Call Instance
     * @return int     Exit Code
     */
    private function handlePostCall(APICall $api_call): int
    {
        $result = $api_call->postCall($this->cli_parameters['post'], $this->cli_parameters['arguments'], false);
        print json_encode(json_decode($result), JSON_PRETTY_PRINT);
        print \PHP_EOL;
        return 0;
    }

    /**
     * Handles the DELETE API call
     *
     * @param  APICall $api_call API Call Instance
     * @return int     Exit Code
     */
    private function handleDeleteCall(APICall $api_call): int
    {
        $result = $api_call->deleteCall($this->cli_parameters['delete'], false);
        print json_encode(json_decode($result), JSON_PRETTY_PRINT);
        print \PHP_EOL;
        return 0;
    }
    
    /**
     * Show the help message.
     */
    private function showHelp(): void
    {
        $this->printVersionString();

        print 'Usage: simplegitlabapi [--options]' . \PHP_EOL;
        print \PHP_EOL;
        print 'Options:' . \PHP_EOL;
        
        foreach (self::LONG_OPTIONS as $key => $value) {
            $option_line = "  --$key";            
            $option_line .= ($value['arg'] === null) ? "" : "=<".$value['arg'].">";
            $option_line = str_pad($option_line, 30) . $value['desc'] . \PHP_EOL;
            print $option_line;
        }
        print \PHP_EOL;
    }

    /**
     * Prints version infromation.
     * 
     * @todo Add version number as function argument.
     */
    private function printVersionString(): void
    {
        print "This is a development version and has currently no version number." . \PHP_EOL . \PHP_EOL;
    }

    /**
     * Checks if option dependencies are fulfilled.
     * 
     * @todo Add check that only the option of the requirements are asserted.
     * 
     * @param array $options Options from CLI execution.
     */
    private function checkOptionDependencies(array $options): bool
    {
        foreach (array_keys($options) as $key) {
        
            if (self::LONG_OPTIONS[$key]['depends_on'] !== null) {
            
                $requirements = self::LONG_OPTIONS[$key]['depends_on'];
                
                foreach ($requirements as $value) {
                    if (!array_key_exists($value, $options)) return false;                
                }
            
            }
            
        }
        
        return true;
    }

    /**
     * Decodes CLI arguments parameter
     * 
     * @param string $arguments CLI params parameter
     * @return array|null Decoded JSON string as associative array or null when decoding failed.
     */
    private function decodeArgumentsString(string $arguments) {        
        return json_decode($arguments, true);
    }

}
