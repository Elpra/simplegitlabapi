<?php declare(strict_types=1);

/**
 * @copyright Tobias Baumann <tobias.baumann@elpra.de>
 * 
 * @license See LICENSE file in root directory
*/

namespace Elpra\SimpleGitlabAPI\API;

use GuzzleHttp;

/**
 * API calling class using Gitlab API v4
 */
class APICall {
   
    /** @var GuzzleHttp\Client Instance of Guzzle HTTP Client */
    private $guzzle_client;
    
    /**
     * Create API call instance with base URL and private token
     * 
     * @param string            $base_url      Base URI to Gitlab API.
     * @param string            $private_token Private token for Gitlab API.
     */
    public function __construct($base_url, $private_token)
    {
        
        $this->guzzle_client = new \GuzzleHttp\Client(array(
            'base_uri' => $this->prepareBaseURL($base_url),
            'headers' => ['PRIVATE-TOKEN' => $private_token]            
        ));

    }

    /**
     * API GET Call
     *
     * @param string $url       Base URI to Gitlab API.
     * @param bool   $assoc     When true, returns array else JSON string.
     * @return string|array Returns either a JSON string or an array. 
     */
    public function getCall(string $url, bool $assoc=true)
    {
        $json_str = (string) $this->guzzle_client->get($url)->getBody();        
        return ($assoc) ? json_decode($json_str, true) : $json_str;
    }

    /**
     * API POST Call
     *
     * @param string $url        Base URI to Gitlab API.
     * @param array  $attributes Gitlab API attributes
     * @param bool   $assoc      When true, returns array else JSON string.
     * @return string|array Returns either a JSON string or an array.
     */
    public function postCall(string $url, array $attributes, bool $assoc=true)
    {
        
        $post_array = array(
            'form_params' => $attributes
        );
        
        $json_str = (string) $this->guzzle_client->post($url, $post_array)->getBody();
        return ($assoc) ? json_decode($json_str, true) : $json_str;
    }

    /**
     * API DELETE Call
     *
     * @param string $url       Base URI to Gitlab API.
     * @param bool   $assoc     When true, returns array else JSON string.
     * @return string|array Returns either a JSON string or an array.
     */
    public function deleteCall(string $url, bool $assoc=true)
    {
        $json_str = (string) $this->guzzle_client->delete($url)->getBody();
        return ($assoc) ? json_decode($json_str, true) : $json_str;
    }
    
    /**
     * Getter for Gitlab API base URL
     *
     * @return string Base URL
     */
    public function getBaseURL()
    {        
        $http_client_config = $this->guzzle_client->getConfig();        
        return $http_client_config['base_uri'];
    }

    /**
     * Getter for Gitlab API private token
     * 
     * @return string Private Token
     */
    public function getPrivateToken() {        
        $http_client_config = $this->guzzle_client->getConfig();       
        return $http_client_config['headers']['PRIVATE-TOKEN'];        
    }

    /**
     * Ensure that URL ends with an `/`
     *
     * @param  string $base_url Base URL to Gitlab API.
     * @return string           Prepared base URL.
     */
    private function prepareBaseURL($base_url) {
        return (substr($base_url, -1) == '/') ? $base_url : $base_url . '/';
    }
    
}